# encoding: utf-8

from __future__ import division
import os
from random import randint

if __name__ == "__main__":
    import codecs
    from renderer import Renderer

    debug_width = randint(50, 1000)
    scale = 1194 / 2608
    debug_height = debug_width / scale

    # цифры взяты с пикчи на http://rublacklist.net/
    r = Renderer(
        lock=3282,
        unlock=3254,
        illegal=70786,
        all=74068,
        font_paths={
            u'regular': os.path.join('resources', 'arimo', 'Arimo-Regular.ttf'),
            u'bold': os.path.join('resources', 'arimo', 'Arimo-Bold.ttf')
        },
        # resolution=(int(debug_width), int(debug_height))  # debug
    )
    img_as_base64 = r.render_png()
    example_html = [
        u'<!DOCTYPE html>',
        u'\n<html>',
        u'<head lang="ru">',
        u'\t<meta charset="UTF-8">',
        u'\t<title>Пример как эмбеддить</title>',
        u'</head>',
        u'\n<body>',
        u'<div align="left">',
        u'<a href="http://visual.rublacklist.net/" rel="Реестр запрещенных сайтов" target="_self">',
        # (здесь пример как заэмбеддить и поскейлить на клиенте)
        u'<img title="Реестр запрещенных сайтов (РосКомСвобода)" src="data:image/png;base64,{}" width="240px" height="524px" alt="Реестр запрещенных сайтов (РосКомСвобода)"  />'.format(
            img_as_base64),
        u'</a>',
        u'</div>',
        u'</body>',
        u'</html>'
    ]
    with codecs.open('example.html', 'w', 'utf-8') as f:
        for line in example_html:
            f.write(u'{}\n'.format(line))

