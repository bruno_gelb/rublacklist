# encoding: utf-8

from __future__ import division
import collections
from PIL import Image, ImageDraw, ImageFont
from cStringIO import StringIO


class Renderer(object):
    def __init__(self,
                 lock,
                 unlock,
                 illegal,
                 all,
                 font_paths,
                 dpi=96,
                 resolution=(1194, 2608),
                 bg_color=u'#ffffff'):
        self.dpi = dpi
        self.W, self.H = resolution
        self.lock = lock
        self.unlock = unlock
        self.illegal = illegal
        self.all = all

        self.bg_color = bg_color

        # описания взяты с пикчи на http://rublacklist.net/
        self.descriptions = {
            u'lock': {  # (чёрный замок)
                        u'text': u'Запрещенных\nстраниц и\nсайтов',
                        u'color': u'#000000'
            },
            u'unlock': {  # (голубой замок)
                          u'text': u'Разблокировано',
                          u'color': u'#008DD4'
            },
            u'illegal': {  # (серый замок)
                           u'text': u'Неправомерно\nсайтов,'
                                    u'\nнаходящихся\nна тех же\nIP адресах,'
                                    u'\nчто и\nзапрещенные\nссылки\nобозначенные\nв реестре',
                           u'color': u'#5B5B5B'
            },
            u'all': {  # болдовая чёрная цифра по центру пикчи
                       u'text': u'Заблокировано',
                       u'color': u'#000000'
            }
        }

        self.font_paths = font_paths

        self.im = None
        self.draw = None

        self.sum_w_shift = 0
        self.sum_h_shift = 0

        self.w_shift = 0
        self.h_shift = 0

        self.h_steps = 15  # насколь частей условно делим картинку по высоте

    def _render_font_block(self, (w_proportion, h_proportion), caption, font_type):
        font_size = 1
        font = ImageFont.truetype(font_type, font_size)
        while font.getsize(caption)[0] < w_proportion * self.W and font.getsize(caption)[1] < h_proportion * self.H:
            font_size += 1
            font = ImageFont.truetype(font_type, font_size)
        font_size -= 1
        font = ImageFont.truetype(font_type, font_size)
        return font, font_size

    def _add_font_block(self, caption, h_shift, color):
        font_type = u'bold' if color != u'#5B5B5B' else u'regular'
        self.sum_h_shift += self.H * self.h_shift * 1.1
        self.h_shift = h_shift
        font, font_size = self._render_font_block((1, self.h_shift), caption, self.font_paths[font_type])
        if caption.startswith(u'в реестре'):
            self.img_font_size = font_size * 0.8
        elif caption.startswith(u'БЛОКИРУЕТСЯ'):
            self.img_big_font_size = font_size
        self.draw.text(((self.W - font.getsize(caption)[0]) / 2, self.sum_h_shift), caption, font=font, fill=color)

    def _add_image(self, h_shift):
        chart_resolution = (self.W, self.H * h_shift)
        sio = self._render_chart(chart_resolution)
        sio.seek(0)
        chart = Image.open(sio)
        self.sum_h_shift += self.H * self.h_shift
        self.h_shift = h_shift - h_shift / self.h_steps
        offset = (int((self.W - chart_resolution[0]) / 2), int(self.sum_h_shift))
        self.im.paste(chart, offset)

    def _render_chart(self, chart_resolution):
        import matplotlib

        matplotlib.use('Agg')
        import matplotlib.pyplot as plt
        from matplotlib import rc

        font = {'family': 'Verdana',
                'weight': 'normal'}
        rc('font', **font)

        in_inches = []
        for pixel in chart_resolution:
            in_inches.append(pixel / self.dpi)

        fig = plt.figure(frameon=False)
        fig.set_size_inches(tuple(in_inches))

        ax = plt.Axes(fig, [0., 0., 1., 1.], axisbg=self.bg_color)
        ax.set_axis_off()
        fig.add_axes(ax)

        axes = collections.OrderedDict()
        axes[u'lock'] = ax.bar(0, self.lock, width=0.9, color=self.descriptions[u'lock'][u'color'])
        axes[u'unlock'] = ax.bar(1, self.unlock, width=0.9, color=self.descriptions[u'unlock'][u'color'])
        axes[u'illegal'] = ax.bar(2, self.illegal, width=0.9, color=self.descriptions[u'illegal'][u'color'])
        plt.ylim(ax.get_ylim()[0], ax.get_ylim()[1] * 1.1)

        # todo зафигачить картинки замков.
        # annotationbbox или вроде того http://stackoverflow.com/a/4860777/699864
        for key, value in axes.iteritems():
            rect = value[0]
            height = rect.get_height()
            text = u'{}'.format(height)
            ax.text(rect.get_x(),
                    height,
                    text,
                    ha='left',
                    va='bottom',
                    fontsize=self.img_font_size,
                    fontweight='bold',
                    color=self.descriptions[key][u'color'])

        ax.text(0,
                1,
                self.descriptions[u'all'][u'text'],
                horizontalalignment='left',
                verticalalignment='top',
                transform=ax.transAxes,
                fontsize=self.img_font_size)

        # todo рендерить эту цифру на картинку средствами pillow, а не mpl.
        # ибо mpl не способен (вроде бы?) посчитать размер в пикселях
        ax.text(0,
                0.9,
                u'{}'.format(self.all),
                horizontalalignment='left',
                verticalalignment='top',
                transform=ax.transAxes,
                fontsize=self.img_big_font_size,
                fontweight='bold')

        extent = ax.get_window_extent().transformed(fig.dpi_scale_trans.inverted())
        sio = StringIO()
        fig.savefig(sio, bbox_inches=extent.expanded(1, 1), dpi=self.dpi, facecolor=self.bg_color)
        return sio

    def render_png(self):
        self.im = Image.new(mode='RGBA', size=(self.W, self.H), color=self.bg_color)
        self.draw = ImageDraw.Draw(self.im)

        self._add_font_block(caption=str(self.illegal), h_shift=2 / self.h_steps, color='red')
        self._add_font_block(caption=u'доменов', h_shift=1 / self.h_steps, color=u'#5B5B5B')
        # todo разбить на два блока по горизонтали разных цветов и размеров
        self._add_font_block(caption=u'или {:.2f}%'.format(self.illegal / self.all * 100.0).replace(u'.', u','),
                             h_shift=1 / self.h_steps,
                             color=u'#5B5B5B')
        self._add_font_block(caption=u'в реестре запрещенных сайтов', h_shift=0.5 / self.h_steps, color=u'#5B5B5B')
        self._add_font_block(caption=u'БЛОКИРУЕТСЯ', h_shift=1 / self.h_steps, color=u'#5B5B5B')
        self._add_font_block(caption=u'НЕПРАВОМЕРНО!', h_shift=1 / self.h_steps, color='red')
        self._add_image(h_shift=7 / self.h_steps)
        self._add_font_block(caption=u'RUBLACKLIST.NET', h_shift=1 / self.h_steps, color=u'#000000')

        sio = StringIO()
        self.im.save(sio, "png")

        return sio.getvalue().encode("base64").strip()

